package com.itau.maratona;

import java.util.ArrayList;

public class App {

	public static void main(String[] args) {
		Arquivo arquivo = new Arquivo("src/alunos.csv");

		ArrayList<Aluno> alunos = new ArrayList<Aluno>();

		alunos = arquivo.ler();

		ArrayList<Equipe> equipes = Sorteador.sortear(alunos, 15);
		
		Imprimir.imprimir(equipes);
	}

}
