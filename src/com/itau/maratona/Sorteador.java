package com.itau.maratona;

import java.util.ArrayList;
import java.util.Collections;

public class Sorteador {
	public static ArrayList<Equipe> sortear(ArrayList<Aluno> alunos, int tamanhoEquipe) {
		ArrayList<Equipe> equipes = new ArrayList<>();
		
		Collections.shuffle(alunos);
		
		int contador = 0;

		if ((alunos.size() % tamanhoEquipe) != 0)
			contador = (alunos.size() / tamanhoEquipe) + 1;
		else
			contador = alunos.size() / tamanhoEquipe;

		while (contador != 0) {
			Equipe equipe = dividir(alunos, tamanhoEquipe, contador);

			equipes.add(equipe);
			
			contador--;
		}
		
		return equipes;
	}

	public static Equipe dividir(ArrayList<Aluno> alunos, int tamanhoEquipe, int numeroEquipe) {
		Equipe equipe = new Equipe();

		if (!(alunos.size() < tamanhoEquipe)) {
			for (int i = 0; i < tamanhoEquipe; i++) {
				Aluno aluno = alunos.remove(0);

				equipe.listaAlunos.add(aluno);
			}
		}
		else {
			while (alunos.size() != 0) {
				Aluno aluno = alunos.remove(0);

				equipe.listaAlunos.add(aluno);
			}
		}
		
		equipe.id = numeroEquipe;
		
		return equipe;
	}
}
