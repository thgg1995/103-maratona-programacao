package com.itau.maratona;

import java.util.ArrayList;

public class Imprimir {
	public static void imprimir(ArrayList<Equipe> equipes) {
		for (int i = 0; i < equipes.size(); i++) {
			System.out.println("Equipe: " + equipes.get(i).id);
			for (int j = 0; j < equipes.get(i).listaAlunos.size(); j++) {
				System.out.println("Integrante: " + equipes.get(i).listaAlunos.get(j).nome);
			}
		}
	}

}
